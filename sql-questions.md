Given these two tables, orders and order_items, answer the follow questions:

**PS1: In order to calculate the total_order, use this formula: "((quantity*price)+provincial_tax+shipping_value)-discount".**

**PS2: You can use any SQL language.**


.


Table name: orders

id|organization_id|order_type    |order_received_date     |crm_account_id|created_by|status           |
--|---------------|--------------|------------------------|--------------|----------|-----------------|
 1|              1|wholesale     |2020-07-23T04:00:00.000Z|             1|        14|approved         |
30|              1|wholesale     |2020-08-25T04:00:00.000Z|            14|        14|approved         |
31|              1|recreational  |2020-08-25T04:00:00.000Z|            15|        14|approved         |
32|              1|wholesale     |2020-08-25T04:00:00.000Z|            16|        14|shipped          |
34|              1|intra-industry|2020-08-25T04:00:00.000Z|            17|        14|approved         |
35|              1|recreational  |2020-08-19T04:00:00.000Z|            15|        14|cancelled        |
36|              1|intra-industry|2020-08-26T04:00:00.000Z|            17|        14|cancelled        |
37|              1|intra-industry|2020-08-26T04:00:00.000Z|            17|        14|approved         |
39|              1|wholesale     |2020-08-27T04:00:00.000Z|            16|        14|approved         |
40|              1|wholesale     |2020-12-07T05:00:00.000Z|            16|        14|cancelled        |
41|              1|patient       |2020-12-31T05:00:00.000Z|             5|        14|awaiting_approval|
42|              1|wholesale     |2020-12-18T05:00:00.000Z|            14|        14|approved         | 
 
.

 

Table name: order_items

id|sku_id|sku_name                       |order_id (order_id --> id) |variety            |quantity|price|provincial_tax|discount|shipping_value|
--|------|-------------------------------|---------------------------|-------------------|--------|-----|--------------|--------|--------------|
 1|     2|Bubba Kush-5 Plants            |                          1|Bubba Kush         |       2| 9.89|          2.57|    0.00|          0.00|
 2|     2|Bubba Kush-5 Plants            |                          1|Bubba Kush         |       1| 9.89|          1.29|    0.00|          0.00|
36|    16|Berry White, Indica-10g Dry    |                         30|Berry White, Indica|       1| 0.51|          0.07|    0.00|          0.00|
37|     3|Berry White-10g Wet            |                         30|Berry White        |       1| 0.29|          0.04|    0.00|          0.00|
38|    17|Berry White, Indica-10 Plants  |                         31|Berry White, Indica|       3| 7.69|          3.00|    0.00|          0.00|
44|    16|Berry White, Indica-10g Dry    |                         32|Berry White, Indica|       1| 0.51|          0.07|    0.00|        100.00|
51|    14|Bubba Kush, Indica-1g Distilled|                         34|Bubba Kush, Indica |       1| 6.46|          0.84|    0.00|          0.00|
50|    15|Bubba Kush, Indica-1g Crude    |                         34|Bubba Kush, Indica |       1| 7.43|          0.97|    0.00|          0.00|
52|     8|Berry White-5g Wet             |                         34|Berry White        |       2| 5.63|          1.46|    0.00|          0.00|
53|    18|Berry White, Indica-2g Dry     |                         36|Berry White, Indica|       4| 0.28|          0.15|    0.00|          0.00|
56|    14|Bubba Kush, Indica-1g Distilled|                         37|Bubba Kush, Indica |       4| 6.46|          3.36|    0.00|          0.00|
54|    18|Berry White, Indica-2g Dry     |                         37|Berry White, Indica|       1| 0.28|          0.04|    0.00|          0.00|
55|    15|Bubba Kush, Indica-1g Crude    |                         37|Bubba Kush, Indica |       2| 7.43|          1.93|    0.00|          0.00|
57|     8|Berry White-5g Wet             |                         37|Berry White        |       1| 5.63|          0.73|    0.00|          0.00|
59|    17|Berry White, Indica-10 Plants  |                         39|Berry White, Indica|       4| 7.69|          4.00|    0.00|          0.00|
60|    16|Berry White, Indica-10g Dry    |                         39|Berry White, Indica|       4| 0.51|          0.27|    0.00|          0.00|
61|     2|Bubba Kush-5 Plants            |                         39|Bubba Kush         |       4| 9.89|          5.14|    0.00|          0.00|
62|     2|Bubba Kush-5 Plants            |                         42|Bubba Kush         |       4| 9.89|          5.14|    0.00|          0.00|


.





1) - Write a SQL query to fetch "id", "organization_id", "order_type", **"total_order"** for all "approved" and "shipped" orders that was received in "Aug 2020" (+++++)
  
select orders.id, orders.organization_id, orders.order_type, format(sum(((order_items.quantity * order_items.price) * (1+order_items.provincial_tax/100)+shipping_value)-discount), 2) as total_order from orders INNER join order_items on orders.id = order_items.order_id where (orders.status = 'approved' or orders.status = 'shipped') and orders.order_received_date like '2020-08%' GROUP by order_items.order_id




2) - Write an SQL query that fetches all the **orders** and how many order items each order has (+++)
   
select orders.id, orders.organization_id, orders.order_type, count(order_items.order_id) as total_items 
from orders INNER join order_items on orders.id = order_items.order_id GROUP by order_items.order_id



3) - Write a SQL query that fetches the unique values of "order_type" from **orders table** (+)
  
 select DISTINCT orders.order_type from orders order by orders.order_type ASC




4) - Write an SQL query that returns the **provincial tax percent** of the order ids "37" and "1" (++)
  
  select orders.id, order_items.provincial_tax from orders INNER join order_items on orders.id = order_items.order_id where orders.id=37 or orders.id=1




5) - Write an SQL query to fetch the **orders** that have duplicated sku_id (+++++)
  
  select orders.id, orders.organization_id, orders.order_type, count(order_items.sku_id) as sku_id_duplicate from orders INNER join order_items on orders.id = order_items.order_id GROUP by order_items.order_id having sku_id_duplicate >= 2


