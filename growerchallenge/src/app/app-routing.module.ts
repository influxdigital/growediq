import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [


  {
    path: '',
    redirectTo: '/first-page',
    pathMatch: 'full',
  },
  {
    path : 'first-page',
    loadChildren: './first-page/first-page.module.ts#firstPageModule',
  },
  {
    path : 'second-page',
    loadChildren: './second-page/second-page.module.ts#secondPageModule',
  }
  
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
