import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstPageComponent } from './first-page.component';
import { firstPageRountingModule } from './first-page.routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    firstPageRountingModule
  ]
})

export class firstPageModule { }
