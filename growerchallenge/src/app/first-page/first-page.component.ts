import { Component, OnInit, ViewChildren, QueryList, AfterViewInit, ViewChild, Inject } from '@angular/core';
import {RolApsService} from './first-page.service';
import { Rolaps } from './first-page';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {FilterService} from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { MatPaginator } from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { $ } from 'protractor';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';



@Component({
  selector: 'app-first-page',
  templateUrl: './first-page.component.html',
  styleUrls: ['./first-page.component.css']
})

export class FirstPageComponent implements OnInit, AfterViewInit  {

  rolaps: any=[];
  items: MenuItem[];
  symbol: string;
  name: string;
  phone_number: string;
  type: string;
  create_user: boolean;

  // dataSource = new MatTableDataSource<PeriodicElement>(this.rolaps);

  
  displayedColumns: string[] = ['name', 'phone_number', 'type'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  create_user_area: boolean;
  user_type_title: string;
  
  ngAfterViewInit(): void {
    this.rolaps.paginator = this.paginator;

  }

  constructor(
    private rolapsService: RolApsService, 
    private filterService: FilterService,
    private http: HttpClient,
    public dialog: MatDialog) { }


    public createUser() {
      this.create_user = false; 
    }

    public leftcreateUser(){
      this.create_user = true; 
    }

    public createNewUser(){
      this.create_user = true; 
    }

    openDialog(): void {
      const dialogRef = this.dialog.open(overviewDialog, {
        width: '250px',
      });
  
      dialogRef.afterClosed().subscribe(result => {
        console.log('Save success!');
      });
    }

    public onChangeUser(val){
      if(val == 2){
        this.create_user_area = true;
        this.user_type_title = "Grower";
      }else{
        this.create_user_area = false;
        this.user_type_title = "Warehouse";
      }
    }


  ngOnInit(): void {
    this.rolapsService.getRolAps().subscribe(
      response => {
        this.rolaps = response;

      },
      error => {
        alert('Houve algum erro ao carregar a lista.');
      }
    );  

    this.create_user = true; 

    this.create_user_area = true;
    this.user_type_title = "Grower";

  }


}

export interface PeriodicElement {
  name: string;
  phone_number: string;
  type: string;
}

@Component({
  selector: 'save-confirmation',
  templateUrl: 'save-confirmation.html',
})
export class overviewDialog {

  constructor(
    public dialogRef: MatDialogRef<overviewDialog>,
    public router: Router) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    this.dialogRef.close();
    this.router.navigate(['second-page'])
    .then(() => {
      window.location.reload();
    });
  }

}