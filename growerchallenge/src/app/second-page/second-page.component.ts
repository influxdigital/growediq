import { Component, OnInit, ViewChildren, QueryList, AfterViewInit, ViewChild, Inject } from '@angular/core';
import {secondPageService} from './second-page.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {FilterService} from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { MatPaginator } from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { $ } from 'protractor';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';



@Component({
  selector: 'app-second-page',
  templateUrl: './second-page.component.html',
  styleUrls: ['./second-page.component.css']
})
export class secondPageComponent implements OnInit {

  rolaps: any=[];
  items: MenuItem[];
  symbol: string;
  name: string;
  phone_number: string;
  type: string;
  create_user: boolean;
  user_type: any;

  // dataSource = new MatTableDataSource<PeriodicElement>(this.rolaps);

  
  displayedColumns: string[] = ['name', 'address','phone_number', 'gender','years_of_experience','educational_qualification','inventory_management_certification','type'];
  user_type_fields: boolean;
  

  constructor(
    private sp: secondPageService, 
    private filterService: FilterService,
    private http: HttpClient,
    public dialog: MatDialog) { }


    public createUser() {
      this.create_user = false; 
    }

    
    public leftcreateUser(){
      this.create_user = true; 
    }

    public createNewUser(){
      this.create_user = true; 
    }

    public onChange(val: string){
      this.user_type = val;
      this.sp.getUsers().subscribe(
        response => {
          this.rolaps = response;
          this.rolaps = this.rolaps.filter(row => row.type === val);

          if(val == "grower"){
            this.user_type_fields = true;
          }else{
            this.user_type_fields = false;
          }

        },
        error => {
          alert('Houve algum erro ao carregar a lista.');
        }
      ); 
    }

  ngOnInit(): void {
    this.sp.getUsers().subscribe(
      response => {
        this.rolaps = response;

      },
      error => {
        alert('Houve algum erro ao carregar a lista.');
      }
    );  

    this.user_type_fields = false;

  }


}
