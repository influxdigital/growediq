import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { HttpClient, HttpHeaders   } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class secondPageService {
  url = "http://5ccc5842f47db800140110d0.mockapi.io/users";


  constructor(private http: HttpClient) {}

  getUsers(): Observable<any[]>{
    // const headers = { 'Authorization': localStorage['token']  }
    return this.http.get<any[]>(this.url);
  }

}
