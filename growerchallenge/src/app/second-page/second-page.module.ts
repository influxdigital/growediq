import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { secondPageComponent } from './second-page.component';
import { secondPageRountingModule} from './second-page.routing.module'



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    secondPageRountingModule
  ]
})
export class secondPageModule { }
